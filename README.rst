LE Sign
=======

Automatic signing and renewal of Let's Encrypt certificates using acme-tiny,
with logging and sending status to Zabbix.

To be set up as a daily cron job.

Install
~~~~~~~

First install acme_tiny_ script to be available in system PATH.

.. _acme_tiny: https://github.com/diafygi/acme-tiny

Put `le-sign` script somewhere to be available in PATH, e.g. `/usr/local/sbin`
and edit some configuration variables inside the script.

Included in the repository are also examples of logrotate config, crontab and
Zabbix template.

