#!/bin/bash

# --- configuration

# where to keep certificates and keys (set permissions accordingly)
CERT_DIR='/etc/letsencrypt'

# directory where to create verification files
# should visible at http://domain/.well-known/acme-challenge/
# for all DNS names requested
WWW_ROOT='/var/www/.well-known/acme-challenge'

# how early to try to renew before expiry
EXPIRES_DAYS=15

# send data to Zabbix monitoring (leave ZABBIX_SERVER empty to disable)
# numeric value sent: 0=ERROR, 1=Renewed, 2=NotNeeded
ZABBIX_SERVER=''
ZABBIX_HOST=`hostname -f`
ZABBIX_KEY='letsencrypt.renew'

LOG='/var/log/letsencrypt.log'

# --- end of configuration


while [[ $1 == -* ]] ; do
	case "$1" in
		"-f")
			FORCE=1
			shift
			;;
		"-m")
			MANUAL=1
			shift
			;;
		"-h")
			HELP=1
			shift
			;;
		"--")
			shift
			break
			;;
	esac
done

PROG="$0"
NAME="$1"
EXPIRES=$(( EXPIRES_DAYS * 86400 ))

function zabbix_send {
	[[ $MANUAL -ne 1 ]] && zabbix_sender -z "$ZABBIX_SERVER" -s "$ZABBIX_HOST" -k "$ZABBIX_KEY" -o "$1"
}

function finish {
	echo -n "/\ /\ Signing of '$NAME' finished (rc=$1) @ "
	date --rfc-3339=seconds
	exit $1
}

function usage {
	echo "use: $PROG [-f] [-m] <name>" >&2
	echo "-f .... force renewal even if not expiring" >&2
	echo "-m .... manual mode, do not log or send status to zabbix" >&2
	echo 'name .. base name of certificate files (CSR named "name.csr" has to exist)' >&2
}

if [[ $HELP -eq 1 ]] ; then
	usage
	exit 0
fi

if [[ $MANUAL -ne 1 ]] ; then
	exec 3>&2 # save stderr as FD 3
	exec &>> "$LOG" # redirect stdout and stderr to $LOG file
fi

echo -n "\/ \/ Signing of '$NAME' started @ "
date --rfc-3339=seconds

if [[ -z "${NAME}" ]] ; then
	echo "FAIL: required argument missing"
	usage
	finish 1
fi

if [[ ! -r "${CERT_DIR}/${NAME}.csr" ]] ; then
	echo "FAIL: ${CERT_DIR}/${NAME}.csr does not exist or is not readable."
	echo "$0 FAIL: ${CERT_DIR}/${NAME}.csr does not exist or is not readable." >&3
	finish 1
fi

if [[ -f "${CERT_DIR}/${NAME}.crt" ]] ; then
	echo "${CERT_DIR}/${NAME}.crt already exists, chceking expiry."
	if ! OUTPUT=$( openssl x509 -checkend $EXPIRES -noout -in "${CERT_DIR}/${NAME}.crt" 2>&1 ) ; then
		# expiry check did not return success
		if [[ -n "$OUTPUT" ]] ; then
			# because of error
			echo "$OUTPUT"
			zabbix_send 0
			finish 1
		else
			# because cert is about to expire
			echo "Certificate expires within the given time period, proceeding to renew."
		fi
	else
		# certificate is good
		echo "Certificate is good, no renewal needed."
		if [[ $FORCE -eq 1 ]] ; then
			echo "Forced to proceed anyway."
		else
			zabbix_send 2
			finish 0
		fi
	fi
fi

/usr/local/sbin/acme_tiny.py \
	--account-key "${CERT_DIR}/account.key" \
	--csr "${CERT_DIR}/${NAME}.csr" \
	--acme-dir "${WWW_ROOT}" \
	> "${CERT_DIR}/${NAME}.crt.new"

RET=$?
if [[ $RET -eq 0 ]] ; then
	mv -v --backup=numbered "${CERT_DIR}/${NAME}.crt.new" "${CERT_DIR}/${NAME}.crt"
	cat "${CERT_DIR}/${NAME}.crt" "${CERT_DIR}/chain.crt" > "${CERT_DIR}/${NAME}_chain.crt"
	zabbix_send 1
else
	rm "${CERT_DIR}/${NAME}.crt.new"
	zabbix_send 0
fi

finish $RET
